﻿using PokerLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PokerConsoleUDPClient
{
    class Program
    {
        //http://www.jarloo.com/c-udp-multicasting-tutorial/
        public static IPAddress senderMulticast = IPAddress.Parse("239.0.0.223");
        public static IPAddress listenerMulticast = IPAddress.Parse("239.0.0.222");
        public static IPEndPoint senderEndPoint = new IPEndPoint(senderMulticast, 2223);
        public static IPEndPoint listenerEndPoint = new IPEndPoint(IPAddress.Any, 2222);
        public static string CurrentMsgToSend;
        private static UdpClient udpSender;
        private static UdpClient udpListener;
        private static byte[] buffer;
        private static Queue<string> receivedMessages;
        private static Thread listenerThread;
        private static Thread senderThread;

        public static bool IsPowered;
        public static int MaxPlayersAtTable;
        public static int ConnectedPlayers;
        public static long GamesPlayed;
        public static string PlayerID;
        public static bool ConnectedToServer;
        public static long CurrentPool;
        public static long MinBet;
        public static long MaxBet;
        public static PlayerInfo playerInfo;
        public static List<PlayerInfo> OtherPlayers;
        public static GameState CurrentGameState;
        public static GameState WhatGameStateClientIs;
        public static long FirstBetAmount;
        public static long SecondBetAmount;
        public static long ThirdBetAmount;
        public static long FourthBetAmount;

        //https://stackoverflow.com/questions/4646827/on-exit-for-a-console-application
        static ConsoleEventDelegate handler;   // Keeps it from getting garbage collected
        private static string LastProcessedMessage;

        // Pinvoke
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

        static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                Console.WriteLine("Console window closing, death imminent");
                CloseListener();
                CloseSender();
            }
            return false;
        }

        static void InitProgram()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            OtherPlayers = new List<PlayerInfo>();
            LastProcessedMessage = "";
            IsPowered = true;
            InitSender();
            InitListener();

            receivedMessages = new Queue<string>();
            CurrentMsgToSend = "";

            Console.WriteLine("Program started.");
            Console.WriteLine("State your id and press enter: (don't use '|' symbol)");
            PlayerID = Console.ReadLine();
            CurrentGameState=GameState.NULL;
            WhatGameStateClientIs=GameState.OutOfLoop;
        }
        static void Main(string[] args)
        {
            handler = new ConsoleEventDelegate(ConsoleEventCallback);
            SetConsoleCtrlHandler(handler, true);

            InitProgram();
            while (IsPowered)
            {
                GameLoop();
                BuildMessage();
                ProcessServerMSG();
                Thread.Sleep(100);
            }
            Console.WriteLine("Finished application. Press enter.");
            Console.ReadLine();
        }

        private static void ProcessServerMSG()
        {
            string s = new string(LastProcessedMessage.ToCharArray());
            string[] split = s.Split('|');
            if (split.Length > 0)
            {
                GamesPlayed = Convert.ToInt32(split[5]);
                ConnectedPlayers = Convert.ToInt32(split[6]);
                CurrentGameState = (GameState) Enum.Parse(typeof (GameState), split[7]);
                CurrentPool = Convert.ToInt64(split[8]);
            }
        }

        private static void BuildMessage()
        {
            CurrentMsgToSend = PlayerID + "|" + WhatGameStateClientIs + "|" + FirstBetAmount + "|" + SecondBetAmount + "|" + ThirdBetAmount + "|" + FourthBetAmount;
        }


        static void InitSender()
        {
            CloseSender();
            udpSender = new UdpClient();
            udpSender.EnableBroadcast = true;
            udpSender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            udpSender.ExclusiveAddressUse = false;
            senderThread = new Thread(SendMessages);
            senderThread.IsBackground = true;
            senderThread.Start();
            Console.WriteLine("Inited Sender");
        }
        static void CloseSender()
        {
            if (senderThread != null)
            {
                senderThread.Abort();
                Console.WriteLine("Closed sender thread.");
            }
            if (udpSender != null)
            {
                udpSender.Close();
                Console.WriteLine("Closed UdpSenderClient.");
            }
        }

        static void InitListener()
        {
            CloseListener();
            udpListener = new UdpClient();
            udpListener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            udpListener.ExclusiveAddressUse = false;
            udpListener.Client.Bind(listenerEndPoint);
            udpListener.JoinMulticastGroup(listenerMulticast);
            listenerThread = new Thread(ListenToMessages);
            listenerThread.IsBackground = true;
            listenerThread.Start();
            Console.WriteLine("Inited Listener");
        }
        static void CloseListener()
        {

            if (listenerThread != null)
            {
                listenerThread.Abort();
                Console.WriteLine("Closed listener thread.");
            }
            if (udpListener != null)
            {
                udpListener.Close();
                Console.WriteLine("Closed UdpListenerClient.");
            }
        }

        static void SendMessages()
        {
            while (IsPowered)
            {
                SendOneMessage();
                Thread.Sleep(100);
            }
        }
        static void SendOneMessage()
        {
            if (!string.IsNullOrEmpty(CurrentMsgToSend))
            {
                buffer = Encoding.UTF32.GetBytes(CurrentMsgToSend);
                udpSender.Send(buffer, buffer.Length, senderEndPoint);
            }
        }

        static void ListenToMessages()
        {
            while (IsPowered)
            {
                ListenOnce();
                Thread.Sleep(100);
            }

        }
        static void ListenOnce()
        {
            Byte[] data = udpListener.Receive(ref listenerEndPoint);
            string strData = Encoding.UTF32.GetString(data);
            if (!string.IsNullOrEmpty(strData))
            {
                receivedMessages.Enqueue(strData);
                if (!string.IsNullOrEmpty(strData))
                {              
                    LastProcessedMessage = strData;
                }
            }
        }
        static void GameLoop()
        {
            Console.Clear();
            Console.WriteLine(DateTime.Now.ToLocalTime());
            Console.WriteLine("Games played: " + GamesPlayed);
            Console.WriteLine("Current pool: " + CurrentPool);
            Console.WriteLine("Players: " + ConnectedPlayers);
            Console.WriteLine("You are: " + PlayerID);
            //Console.WriteLine("Connected to server: "+ConnectedToServer);
            Console.WriteLine("--");
            Console.WriteLine("Current Game State: " + CurrentGameState);
            Console.WriteLine("--");
            Console.WriteLine("Server:");
            Console.WriteLine(LastProcessedMessage);
            Console.WriteLine("--");
            switch (CurrentGameState)
            {
                case GameState.SetupPrepareTable:
                    WhatGameStateClientIs=GameState.SetupPrepareTable;
                    break;
                case GameState.SetupDealCards:
                    WhatGameStateClientIs = GameState.SetupDealCards;
                    break;
                case GameState.SetupBets:
                    WhatGameStateClientIs = GameState.SetupBets;
                    break;
                case GameState.FlopDealCards:
                    WhatGameStateClientIs = GameState.FlopDealCards;
                    break;
                case GameState.FlopBets:
                    WhatGameStateClientIs = GameState.FlopBets;
                    break;
                case GameState.TurnDealCards:
                    WhatGameStateClientIs = GameState.TurnDealCards;
                    break;
                case GameState.TurnBets:
                    WhatGameStateClientIs = GameState.TurnBets;
                    break;
                case GameState.RiverDealCards:
                    WhatGameStateClientIs = GameState.RiverDealCards;
                    break;
                case GameState.RiverBets:
                    WhatGameStateClientIs = GameState.RiverBets;
                    break;
                case GameState.Showdown:
                    WhatGameStateClientIs = GameState.Showdown;
                    break;
                case GameState.ResetTable:
                    WhatGameStateClientIs = GameState.ResetTable;
                    break;
            }
        }
  
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using PokerLogic;

namespace PokerUDPServer
{

    class Program
    {
        public static bool IsPowered;
        public static bool GameMode;
        public static GameState CurrentGameState;
        public static bool RenderingGameProgressOnScreen=true;
        public static int MaxPlayersAtTable;
        public static long ChipsToStartWith;
        public static long MinBet;
        public static long MaxBet;
        public static bool PlayOnlyWithFullTable;
        public static List<string> CurrentCardDeck;
        public static List<string> CurrentHouseCards;
        public static long CurrentChipPool;

        public static int ConnectedPlayers
        {
            get { return ConnectedPlayersDict.Count; }
        }
        public static long GamesPlayed;
        public static long AllConnectionsFromTheStart;
        public static Dictionary<string, PlayerInfo> ConnectedPlayersDict;

        //http://www.jarloo.com/c-udp-multicasting-tutorial/
        public static IPAddress senderMulticast = IPAddress.Parse("239.0.0.222");
        public static IPAddress listenerMulticast = IPAddress.Parse("239.0.0.223");
        public static IPEndPoint senderEndPoint = new IPEndPoint(senderMulticast, 2222);
        public static IPEndPoint listenerEndPoint = new IPEndPoint(IPAddress.Any, 2223);
        private static UdpClient udpSender;
        private static UdpClient udpListener;
        private static byte[] buffer;
        private static string CurrentMessageToSend;
        private static Queue<string> receivedMessages;
        private static Thread listenerThread;
        private static Thread senderThread;

        private static List<string> orderedCardDeck;

        //https://stackoverflow.com/questions/4646827/on-exit-for-a-console-application
        static ConsoleEventDelegate handler;   // Keeps it from getting garbage collected
        // Pinvoke
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

        static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                Console.WriteLine("Console window closing, death imminent");
                CloseListener();
                CloseSender();
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            handler = new ConsoleEventDelegate(ConsoleEventCallback);
            SetConsoleCtrlHandler(handler, true);

            InitProgram();
            while (IsPowered)
            {
                BuildMessage();
                ProcessMessages();
                GameLoop();
                Thread.Sleep(100);
            } 
            Console.WriteLine("Finished application. Press enter.");
            Console.ReadLine();
        }

        static void InitProgram()
        {
            ConnectedPlayersDict = new Dictionary<string, PlayerInfo>();
            MaxPlayersAtTable = 2;
            GamesPlayed = 0;
            ChipsToStartWith = 100;
            MinBet = 2;
            GameMode = false;
            CurrentGameState = GameState.SetupPrepareTable;
            receivedMessages = new Queue<string>();
            IsPowered = true;
            orderedCardDeck = PokerGameLogic.CreateOrderedCardDeck();
            CurrentCardDeck=new List<string>();
            CurrentHouseCards=new List<string>();
            InitSender();
            InitListener();
            while (SetOptions())
            {
                Thread.Sleep(100);
            }
            Console.WriteLine("Program started.");

        }
        static bool SetOptions()
        {

            Console.WriteLine("--");
            Console.WriteLine("Set special options to be avail. when server is running:");
            Console.WriteLine("");
            Console.WriteLine("(1) Show game progress on server screen [" + (RenderingGameProgressOnScreen ? "ON" : "OFF") + "]");
            Console.WriteLine("(2) Max players at the table: " + MaxPlayersAtTable);
            Console.WriteLine("(3) Amount of chips to start With: " + ChipsToStartWith);
            Console.WriteLine("(4) Minimum Bet: (Big blind will be the same amount) " + MinBet);
            Console.WriteLine("(5) Maximum Bet: (set to 0 if there is no max) " + MaxBet);
            Console.WriteLine("(6) Start new game only if there is a full table:" + PlayOnlyWithFullTable);
            Console.WriteLine("");
            Console.WriteLine("Press enter to finish.");
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.D1)
            {
                RenderingGameProgressOnScreen = !RenderingGameProgressOnScreen;
            }
            if (key.Key == ConsoleKey.D2)
            {
                MaxPlayersAtTable = Math.Max(2, Math.Min(MaxPlayersAtTable + 1, 10))%10; //max players is 9+dealer
            }
            if (key.Key == ConsoleKey.D3)
            {
                Console.WriteLine("Enter amount of chips:");
                string resp = Console.ReadLine();
                long chips;
                if (long.TryParse(resp, out chips))
                {
                    ChipsToStartWith = chips;
                }

            }
            if (key.Key == ConsoleKey.D4)
            {
                Console.WriteLine("Enter amount of chips:");
                string resp = Console.ReadLine();
                long chips;
                if (long.TryParse(resp, out chips))
                {
                    MinBet = Math.Max(1,chips);
                }

            }
            if (key.Key == ConsoleKey.D5)
            {
                Console.WriteLine("Enter amount of chips:");
                string resp = Console.ReadLine();
                long chips;
                if (long.TryParse(resp, out chips))
                {
                    MaxBet = Math.Max(0,(chips));
                }

            }
            if (key.Key == ConsoleKey.D6)
            {
                PlayOnlyWithFullTable = !PlayOnlyWithFullTable;
            }
            Console.Clear();
            return key.Key != ConsoleKey.Enter;
        }

        static void InitSender()
        {
            CloseSender();
            udpSender = new UdpClient();
            udpSender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            udpSender.ExclusiveAddressUse = false;
            senderThread = new Thread(SendMessages);
            senderThread.IsBackground = true;
            senderThread.Start();
            Console.WriteLine("Inited Sender");
        }
        static void CloseSender()
        {
            if (senderThread != null)
            {
                senderThread.Abort();
                Console.WriteLine("Closed sender thread.");
            }
            if (udpSender != null)
            {
                udpSender.Close();
                Console.WriteLine("Closed UdpSenderClient.");
            }
        }

        static void InitListener()
        {
            CloseListener();
            udpListener = new UdpClient();
            udpListener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            udpListener.ExclusiveAddressUse = false;
            udpListener.Client.Bind(listenerEndPoint);
            udpListener.JoinMulticastGroup(listenerMulticast);
            listenerThread = new Thread(ListenToMessages);
            listenerThread.IsBackground = true;
            listenerThread.Start();
            Console.WriteLine("Inited Listener");
        }

        static void CloseListener()
        {

            if (listenerThread != null)
            {
                listenerThread.Abort();
                Console.WriteLine("Closed listener thread.");
            }
            if (udpListener != null)
            {
                udpListener.Close();
                Console.WriteLine("Closed UdpListenerClient.");
            }
        }
        static void SendMessages()
        {
            while (IsPowered)
            {
                SendOneMessage();
                Thread.Sleep(100);
            }
        }
        static void SendOneMessage()
        {
            if (!string.IsNullOrEmpty(CurrentMessageToSend))
            {
                buffer = Encoding.UTF32.GetBytes(CurrentMessageToSend);
                udpSender.Send(buffer, buffer.Length, senderEndPoint);           
            }
        }

        private static void BuildMessage()
        {
            string msg = "SERVER|" +DateTime.Now.ToLocalTime()+"|"+ MinBet + "|" + MaxBet + "|" + MaxPlayersAtTable + "|" + GamesPlayed + "|" + ConnectedPlayers + "|" + CurrentGameState + "|" +
                CurrentChipPool + "|" + PlayerInfo.HandToString(CurrentHouseCards);
            if (ConnectedPlayers > 0)
                msg += "|";
            int i = 0;
            foreach (KeyValuePair<string,PlayerInfo> item in ConnectedPlayersDict)
            {
                msg += item.Value.Serialize();
                if (i < ConnectedPlayers - 1)
                    msg += "|";
                i++;
            }
            CurrentMessageToSend = msg;
        }
        private static void ProcessMessages()
        {
            while (receivedMessages.Count > 0)
            {
                string msg = receivedMessages.Dequeue();
                string[] split = msg.Split('|');
                string id = split[0];
                if (ConnectedPlayersDict.ContainsKey(id))
                {
                    PlayerInfo player = ConnectedPlayersDict[id];
                    player.LastMsgTime = DateTime.Now;
                    player.WhatStatePlayerIsIn = (GameState)Enum.Parse(typeof(GameState), split[1]);
                }
                else
                {
                    ConnectedPlayersDict.Add(id,new PlayerInfo(id,ChipsToStartWith));
                    AllConnectionsFromTheStart++;
                }

            }
        }
        static void ListenToMessages()
        {
            while (IsPowered)
            {
                ListenOnce();
                Thread.Sleep(100);
            }
            
        }
        static void ListenOnce()
        {
            Byte[] data = udpListener.Receive(ref listenerEndPoint);
            string strData = Encoding.UTF32.GetString(data);
            if (!string.IsNullOrEmpty(strData))
            {
                receivedMessages.Enqueue(strData);
            }
        }
  
        static void GameLoop()
        {
            Console.Clear();
            Console.WriteLine(DateTime.Now.ToLocalTime());
            Console.WriteLine("--");
            Console.WriteLine("Minimum Bet: " + MinBet);
            Console.WriteLine("Maximum Bet: " + MaxBet);
            Console.WriteLine("Small Blind: " + (int)(MinBet / 2));
            Console.WriteLine("Big Blind: " + MinBet);
            Console.WriteLine("--");
            Console.WriteLine("Games played: " + GamesPlayed);
            Console.WriteLine("POOL: " + CurrentChipPool);
            Console.WriteLine("Players: (" + ConnectedPlayers+")");
            foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
            {
                Console.WriteLine(pair.Key+": "+pair.Value.Chips+" ("+pair.Value.WhatStatePlayerIsIn+")");
            }
            Console.WriteLine("--");
            Console.WriteLine("Current GameState: " + CurrentGameState);
            if (RenderingGameProgressOnScreen)
            {
                RenderGameScreen();
            }
            if (!CheckIfPlayersAreSyncedUp())
            {
                return;
            }
            switch (CurrentGameState)
            {
                case GameState.SetupPrepareTable:
                    SetupPrepareTableLogic();
                    break;
                case GameState.SetupDealCards:
                    SetupDealCardsLogic();
                    break;
                case GameState.SetupBets:
                    SetupBetsLogic();
                    break;
                case GameState.FlopDealCards:
                    FlopDealCardsLogic();
                    break;
                case GameState.FlopBets:
                    FlopBetsLogic();
                    break;
                case GameState.TurnDealCards:
                    TurnDealCardsLogic();
                    break;
                case GameState.TurnBets:
                    TurnBetsLogic();
                    break;
                case GameState.RiverDealCards:
                    RiverDealCardsLogic();
                    break;
                case GameState.RiverBets:
                    RiverBetsLogic();
                    break;
                case GameState.Showdown:
                    ShowdownLogic();
                    break;
                case GameState.ResetTable:
                    ResetTableLogic();
                    break;
            }
        }

        static bool CheckIfPlayersAreSyncedUp()
        {

            foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
            {
                PlayerInfo player = pair.Value;
                if (player.IsPlaying)
                {
                    if (player.WhatStatePlayerIsIn != CurrentGameState)
                    {
                        Console.WriteLine("Player "+player.ID+" is not synced up!");
                        return false; //need to wait for all players to sync up
                    }
                }
            }
            return true;
        }
        static void RenderGameScreen()
        {
            Console.WriteLine("");
            Console.WriteLine("--");
            string houseCards="HOUSE: ";
            foreach (string currentHouseCard in CurrentHouseCards)
            {
                houseCards += currentHouseCard + ",";
            }
            Console.WriteLine(houseCards);
            Console.WriteLine("--");
            foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
            {
                PlayerInfo player = pair.Value;
                if(player.IsPlaying)
                    Console.WriteLine(player.ID+": "+player.Card1+", "+player.Card2);
            }           
        }
        static void SetupPrepareTableLogic()
        {
            if ((PlayOnlyWithFullTable&&ConnectedPlayers < MaxPlayersAtTable)||ConnectedPlayers < 2)
            {
                Console.WriteLine("Waiting for more players before starting.");
                return;
            }
            else
            {
                CurrentChipPool = 0;
                CurrentCardDeck.Clear();
                CurrentCardDeck = PokerGameLogic.CreateShuffledCardDeck();
                CurrentHouseCards.Clear();
                foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
                {
                    PlayerInfo player = pair.Value;
                    player.Card1 = "";
                    player.Card2 = "";
                    player.IsPlaying = true;
                }
                CurrentGameState=GameState.SetupDealCards;
            }
        }

        static void SetupDealCardsLogic()
        {
            Console.WriteLine("Dealing cards.");
            foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
            {
                PlayerInfo player = pair.Value;
                player.Card1 = CurrentCardDeck[0];
                CurrentCardDeck.RemoveAt(0);
                player.Card2 = CurrentCardDeck[0];
                CurrentCardDeck.RemoveAt(0);
            }     
            CurrentGameState=GameState.SetupBets;
        }
        static void SetupBetsLogic()
        {
            Console.WriteLine("Asking for bets:");

            CurrentGameState = GameState.FlopDealCards;
        }
        static void FlopDealCardsLogic()
        {
            Console.WriteLine("Dealing cards.");
            CurrentHouseCards.Add(CurrentCardDeck[0]);
            CurrentCardDeck.RemoveAt(0);
            CurrentHouseCards.Add(CurrentCardDeck[0]);
            CurrentCardDeck.RemoveAt(0);
            CurrentHouseCards.Add(CurrentCardDeck[0]);
            CurrentCardDeck.RemoveAt(0);
            CurrentGameState = GameState.FlopBets;
        }
        static void FlopBetsLogic()
        {
            Console.WriteLine("Asking for bets:");

            CurrentGameState = GameState.TurnDealCards;
        }
        static void TurnDealCardsLogic()
        {
            Console.WriteLine("Dealing card.");
            CurrentHouseCards.Add(CurrentCardDeck[0]);
            CurrentCardDeck.RemoveAt(0);
            CurrentGameState = GameState.TurnBets;
        }
        static void TurnBetsLogic()
        {
            Console.WriteLine("Asking for bets:");

            CurrentGameState = GameState.RiverDealCards;
        }
        static void RiverDealCardsLogic()
        {
            Console.WriteLine("Dealing card.");
            CurrentHouseCards.Add(CurrentCardDeck[0]);
            CurrentCardDeck.RemoveAt(0);
            CurrentGameState = GameState.RiverBets;
        }
        static void RiverBetsLogic()
        {
            Console.WriteLine("Asking for bets:");

            CurrentGameState = GameState.Showdown;
        }
        static void ShowdownLogic()
        {
            Console.WriteLine("Showdown!");
            Dictionary<int, List<PlayerInfo>> dict = new Dictionary<int, List<PlayerInfo>>();
            for (int i = 0; i < 11; i++)
            {
                dict[i] = new List<PlayerInfo>();
            }
            foreach(KeyValuePair<string,PlayerInfo> pair in ConnectedPlayersDict)
            {
                PlayerInfo playerInfo = pair.Value;
                List<string> allCards=new List<string>(CurrentHouseCards);
                allCards.Add(playerInfo.Card1);
                allCards.Add(playerInfo.Card2);
                playerInfo.BestHand = PokerGameLogic.GetBestHandFromAllCards(allCards);
                playerInfo.BestHandValue = PokerGameLogic.GetPosValueOfHand(playerInfo.BestHand);

                dict[playerInfo.BestHandValue].Add(playerInfo);
                Console.WriteLine(playerInfo.ID + ": "+PlayerInfo.HandToString(playerInfo.BestHand)+" (" + playerInfo.BestHandValue + ")");
            }

            List<PlayerInfo> bestValue=new List<PlayerInfo>();
            for (int i = 10; i >= 0; i--)
            {
                if (dict[i].Count>0)
                {
                    bestValue = dict[i];
                    break;
                }
            }
            if(bestValue.Count>1)
            {
                //TODO: trzeba sprawdzic kto ma wyzsze karty bo sa te same uklady 
            }
            if (bestValue.Count == 1)
            {
                Console.WriteLine("Winner: " + bestValue[0].ID);
                Console.WriteLine("Won: " + CurrentChipPool);
            } else
            {
                Console.WriteLine("Draw!");
                Console.WriteLine("Between:");
                foreach (PlayerInfo player in bestValue)
                {
                    Console.WriteLine(player.ID);
                }
                long chips = CurrentChipPool / bestValue.Count;
                Console.WriteLine("Each taking: " + chips);
            }
            Console.WriteLine("---\nPress any key to continue");
            Console.ReadKey();
            CurrentGameState = GameState.ResetTable;
        }

        static void ResetTableLogic()
        {
            foreach (KeyValuePair<string, PlayerInfo> pair in ConnectedPlayersDict)
            {
                PlayerInfo player = pair.Value;
                player.IsPlaying = false;
            }
            CurrentGameState=GameState.SetupPrepareTable;
            GamesPlayed++;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerLogic
{
    public enum GameState
    {
        NULL,
        OutOfLoop,
        SetupPrepareTable,
        SetupDealCards,
        SetupBets,
        FlopDealCards,
        FlopBets,
        TurnDealCards,
        TurnBets,
        RiverDealCards,
        RiverBets,
        Showdown,
        ResetTable,
    }
    public static class PokerGameLogic
    {
        private static Random rng = new Random();
        public static List<string> CheckHandForPair(List<string> hand)
        {
            List<string> handRev = new List<string>(hand);
            foreach (string card1 in hand)
            {
                foreach (string card2 in handRev)
                {
                    if(!card1.Equals(card2))
                    {
                        string pos1 = GetPosOfCard(card1);
                        string pos2 = GetPosOfCard(card2);
                        if (pos1.Equals(pos2,StringComparison.OrdinalIgnoreCase))
                        {
                            List<string> l=new List<string>() {card1, card2};
                            foreach (string c in hand)
                            {
                                if (!c.Equals(card1, StringComparison.OrdinalIgnoreCase)&&!c.Equals(card2, StringComparison.OrdinalIgnoreCase))
                                {
                                    l.Add(c);
                                }
                            }
                            return l;
                        }
                    }
                }
            }
            return null;
        }
        public static List<string> CheckHandForTwoPairs(List<string> hand)
        {
            hand = new List<string>(hand);
            string fCard1 = "";
            string fCard2 = "";
            List<string> firstPair = CheckHandForPair(hand);
            if (firstPair == null)
                return null;
            hand.Remove(firstPair[0]);
            hand.Remove(firstPair[1]);
            List<string> secondPair = CheckHandForPair(hand);
            if (secondPair != null)
            {
                return new List<string>(){firstPair[0],firstPair[1],secondPair[0],secondPair[1],secondPair[2]};
            }
            return null;
        }
        public static List<string> CheckHandForTriple(List<string> hand)
        {
            List<string> handRev = new List<string>(hand);
            foreach (string card1 in hand)
            {
                foreach (string card3 in hand)
                {
                    foreach (string card2 in handRev)
                    {
                        if (!card1.Equals(card2)&&!card1.Equals(card3)&&!card2.Equals(card3))
                        {
                            string pos1 = GetPosOfCard(card1);
                            string pos2 = GetPosOfCard(card2);
                            string pos3 = GetPosOfCard(card3);
                            if (pos1.Equals(pos2, StringComparison.OrdinalIgnoreCase)&&pos1.Equals(pos3,StringComparison.OrdinalIgnoreCase))
                            {
                                List<string> l = new List<string>(){card1,card2,card3};
                                foreach (string c in hand)
                                {
                                    if (!c.Equals(card1, StringComparison.OrdinalIgnoreCase) && !c.Equals(card2, StringComparison.OrdinalIgnoreCase) && !c.Equals(card3, StringComparison.OrdinalIgnoreCase))
                                    {
                                        l.Add(c);
                                    }
                                }
                                return l;
                            }
                        }
                    }
                }
            }
            return null;

        }
        public static List<string> CheckHandForStraight(List<string> hand)
        {

            int v1 = GetPosValueOfCard(hand[0]);
            int v2 = GetPosValueOfCard(hand[1]);
            int v3 = GetPosValueOfCard(hand[2]);
            int v4 = GetPosValueOfCard(hand[3]);
            int v5 = GetPosValueOfCard(hand[4]);
            List<int> handVals = new List<int> { v1, v2, v3, v4, v5 };
            handVals.Sort();
            handVals.Reverse();
            int value = handVals[0];
            for (int i = 0; i < handVals.Count-1; i++)
            {
                value -= handVals[i + 1];
                if (value != 1)
                    return null;
                value = handVals[i + 1];
            }
            return hand;
        }
        public static List<string> CheckHandForFlush(List<string> hand)
        {
            string v1 = GetColorOfCard(hand[0]);
            string v2 = GetColorOfCard(hand[1]);
            string v3 = GetColorOfCard(hand[2]);
            string v4 = GetColorOfCard(hand[3]);
            string v5 = GetColorOfCard(hand[4]);
            List<string> handVals = new List<string> { v1, v2, v3, v4, v5 };
            handVals.Sort();
            handVals.Reverse();
            string value = handVals[0];
            for (int i = 1; i < handVals.Count - 1; i++)
            {
                if (value != handVals[i+1])
                    return null;
            }
            return hand;
        }

        public static List<string> CheckHandForFullHouse(List<string> hand)
        {
            hand = new List<string>(hand);
            List<string> first = CheckHandForTriple(hand);
            if (first == null)
                return null;
            hand.Remove(first[0]);
            hand.Remove(first[1]);
            hand.Remove(first[2]);
            List<string> second = CheckHandForPair(hand);
            if (second == null)
                return null;
            return first;


        }

        public static List<string> CheckHandForFourOfKind(List<string> hand)
        {

            List<string> handRev = new List<string>(hand);
            foreach (string card1 in hand)
            {
                foreach (string card3 in hand)
                {
                    foreach (string card2 in handRev)
                    {
                        foreach (string card4 in handRev)
                        {
                            if (!card1.Equals(card2) && !card1.Equals(card3) &&!card1.Equals(card4) && !card2.Equals(card3) && !card2.Equals(card4) && !card3.Equals(card4))
                            {
                                string pos1 = GetPosOfCard(card1);
                                string pos2 = GetPosOfCard(card2);
                                string pos3 = GetPosOfCard(card3);
                                string pos4 = GetPosOfCard(card4);
                                if (pos1.Equals(pos2, StringComparison.OrdinalIgnoreCase) &&
                                    pos1.Equals(pos3, StringComparison.OrdinalIgnoreCase)&&pos1.Equals(pos4,StringComparison.OrdinalIgnoreCase))
                                {
                                    List<string> l = new List<string>() {card1, card2, card3,card4};
                                    foreach (string c in hand)
                                    {
                                        if (!c.Equals(card1, StringComparison.OrdinalIgnoreCase) && !c.Equals(card2, StringComparison.OrdinalIgnoreCase) && !c.Equals(card3, StringComparison.OrdinalIgnoreCase) && !c.Equals(card4, StringComparison.OrdinalIgnoreCase))
                                        {
                                            l.Add(c);
                                        }
                                    }
                                    return l;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        public static List<string> CheckHandForStraightFlush(List<string> hand)
        {
            List<string> straight = CheckHandForStraight(hand);
            List<string> flush = CheckHandForFlush(hand);
            if(straight==null || flush==null)
                return null;
            return hand;
        }

        public static List<string> CheckHandForRoyalFlush(List<string> hand)
        {
            List<string> sf = CheckHandForStraightFlush(hand);
            if (sf == null)
                return null;
            List<string> aces = new List<string>() {"A♠","A♣","A♥","A♦"};
            foreach (string ace in aces)
            {
                if (hand.Contains(ace))
                    return hand;
            }
            return null;
        }

        public static int GetPosValueOfHand(List<string> hand)
        {
            List<string> checkedHand = CheckHandForRoyalFlush(hand);
            if (checkedHand != null)
                return 10;
            checkedHand = CheckHandForStraightFlush(hand);
            if (checkedHand != null)
                return 9;
            checkedHand = CheckHandForFourOfKind(hand);
            if (checkedHand != null)
                return 8;
            checkedHand = CheckHandForFullHouse(hand);
            if (checkedHand != null)
                return 7;
            checkedHand = CheckHandForFlush(hand);
            if (checkedHand != null)
                return 6;
            checkedHand = CheckHandForStraight(hand);
            if (checkedHand != null)
                return 5;
            checkedHand = CheckHandForTriple(hand);
            if (checkedHand != null)
                return 4;
            checkedHand = CheckHandForTwoPairs(hand);
            if (checkedHand != null)
                return 3;
            checkedHand = CheckHandForPair(hand);
            if (checkedHand != null)
                return 2;
            return 0;

        }

        public static List<string> GetBestHandFromAllCards(List<string> allCards)
        {
            Dictionary<int,List<string>> dict=new Dictionary<int, List<string>>();
            foreach (string card1 in allCards)
            {

                foreach (string card2 in allCards)
                {

                    foreach (string card3 in allCards)
                    {

                        foreach (string card4 in allCards)
                        {

                            foreach (string card5 in allCards)
                            {
                                if (card1.Equals(card2)||card1.Equals(card3)||card1.Equals(card4)||card1.Equals(card5)||
                                    card2.Equals(card3) || card2.Equals(card4) || card2.Equals(card5)||
                                     card3.Equals(card4) || card3.Equals(card5) || card4.Equals(card5))
                                    continue;
                                List<string> hand = new List<string>() {card1,card2,card3,card4,card5};
                                if(hand.Count<5)
                                {
                                    Console.WriteLine("ERROR1");
                                }
                                int value = PokerGameLogic.GetPosValueOfHand(hand);
                                //TODO: zapisywac wszystkie i potem porownac pod wzgledem najwyzszych kart
                               
                                if (dict.ContainsKey(value))
                                    dict[value] = hand;
                                else
                                    dict.Add(value,hand);
                            }
                        }
                    }
                }
            }
            for (int i = 10; i>=0; i--)
            {
                if (dict.ContainsKey(i))
                {
                    if(dict[i].Count!=5)
                    {
                        Console.WriteLine("ERROR2");
                    }
                    return dict[i];
                }
            }
            return null;
        }
        public static int GetPosValueOfCard(string card)
        {
            string pos1 = GetPosOfCard(card);
            return (pos1.Equals("J") ? 11 : (pos1.Equals("Q") ? 12 : (pos1.Equals("K") ? 13 : (pos1.Equals("A") ? 14 : (int.Parse(pos1))))));

        }
        public static string GetColorOfCard(string card)
        {
            return (card.Length > 2 ? card[2] + "" : card[1]+"");
        }
        public static string GetPosOfCard(string card)
        {
            return card[0] + (card.Length > 2 ? card[1] + "" : "");
        }
        public static List<string> CreateOrderedCardDeck()
        {
            return new List<string>()
            {
                "2♠","2♣","2♥","2♦",
                "3♠","3♣","3♥","3♦",
                "4♠","4♣","4♥","4♦",
                "5♠","5♣","5♥","5♦",
                "6♠","6♣","6♥","6♦",
                "7♠","7♣","7♥","7♦",
                "8♠","8♣","8♥","8♦",
                "9♠","9♣","9♥","9♦",
                "10♠","10♣","10♥","10♦",
                "J♠","J♣","J♥","J♦",
                "Q♠","Q♣","Q♥","Q♦",
                "K♠","K♣","K♥","K♦",
                "A♠","A♣","A♥","A♦",
            };
        }

        public static List<string> CreateShuffledCardDeck()
        {
            return (List<string>) Shuffle(CreateOrderedCardDeck());
        } 
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }
    }
}

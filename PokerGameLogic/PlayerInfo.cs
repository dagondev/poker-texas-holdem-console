﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerLogic
{
    public class PlayerInfo
    {
        public string ID;
        public long Chips;
        public long Wins;
        public long Loses;
        public string Card1;
        public string Card2;
        public DateTime LastMsgTime;
        public bool IsPlaying;
        public List<string> BestHand;
        public int BestHandValue;
        public GameState WhatStatePlayerIsIn;

        public PlayerInfo(string id, long chips)
        {
            ID = id;
            Chips = chips;
            Card1 = "";
            Card2 = "";
            WhatStatePlayerIsIn = GameState.NULL;
        }
        public PlayerInfo(string serializedData)
        {
            WhatStatePlayerIsIn = GameState.NULL;
            PopulateWithSerializedData(serializedData);
        }
        public string Serialize()
        {
            return ID + "|" + Chips + "|" + Wins + "|" + Loses + "|" + Card1 + "|" + Card2 + "|" + IsPlaying + "|" + HandToString(BestHand) + "|" + BestHandValue+"|"+WhatStatePlayerIsIn;
        }

        public void PopulateWithSerializedData(string serializedData)
        {
            string[] split = serializedData.Split('|');
            ID = split[0];
            Chips = long.Parse(split[1]);
            Wins = long.Parse(split[2]);
            Loses = long.Parse(split[3]);
            Card1 = split[4];
            Card2 = split[5];
            IsPlaying = bool.Parse(split[6]);
            BestHand = StringToHand(split[7]);
            BestHandValue = int.Parse(split[8]);
            WhatStatePlayerIsIn = (GameState)Enum.Parse(typeof(GameState), split[9]);
        }

        public static List<string> StringToHand(string hand)
        {
            return new List<string>();
        } 
        public static string HandToString(List<string> hand)
        {
            if (hand == null)
                return "HAND NULL!";
            string s = "";
            for (int i = 0; i < hand.Count; i++)
            {
                s += hand[i];
                if (i < hand.Count - 1)
                    s += ", ";
            }
            return s;
        }
    }
}
